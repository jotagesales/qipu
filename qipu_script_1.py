#
"""
@author: Jotagê Sales
@date: 10/11/18
@organization: Jotagê Sales
@copyright: 2018 todos os direitos reservados.
"""
from qipu.numbers import to_words

number = input('Informe o número: \n')
try:
    words = to_words(float(number))
    print(words)
except ValueError:
    print('Valor inválido, tente novamente')
