clean:
	@echo "Execute cleaning ..."
	rm -f *.pyc
	rm -f coverage.xml
	rm -rf qipu/__pycache__
	rm -rf tests/__pycache__

pep8:
	@find . -type f -not -path "*./.venv/*" -not -path "*tests/__init__.py*" -name "*.py"|xargs flake8 --max-line-length=120 --ignore=E402 --max-complexity=6


tests: clean pep8
	py.test --cov=qipu --cov-report=xml tests -v

build-image: clean
	docker build -t qipu .