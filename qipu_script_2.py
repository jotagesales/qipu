"""
@author: Jotagê Sales
@date: 10/11/18
@organization: Jotagê Sales
@copyright: 2018 todos os direitos reservados.
"""

from qipu.aisweb import AisWeb
from terminaltables import AsciiTable


def taf_metar_table():
    taf_metar_table_data = [
        ['METAR', 'TAF'],
        [ais_web.metar, ais_web.taf]
    ]
    table = AsciiTable(taf_metar_table_data)
    print(table.table)


def sun_information_table():
    sun_information_table_data = [
        ['Nascer do sol', 'Por do sol'],
        [ais_web.sun_information['sunrise'], ais_web.sun_information['sunset']]
    ]

    table = AsciiTable(sun_information_table_data)
    print(table.table)


def carts_table():
    carts_table_data = [
        ['Nome da carta', 'Link'],
    ]
    for cart in ais_web.charts:
        carts_table_data.append([cart['name'], cart['link']])
    table = AsciiTable(carts_table_data)
    print(table.table)


try:
    icao_code = input('Por favor digite o código icao: \n')
    ais_web = AisWeb(icao_code)
    ais_web.load()

    taf_metar_table()
    sun_information_table()
    carts_table()
except IndexError:
    print('Não encontramos informacoes sobre o aeródromo informado.')
except Exception:
    print('Ocorreu um erro interno.')
