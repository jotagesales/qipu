"""
@author: Jotagê Sales
@date: 10/11/18
@organization: Jotagê Sales
@copyright: 2018 todos os direitos reservados.
"""
from unittest import TestCase
from qipu.numbers import to_words


class NumbersTestCase(TestCase):

    def test_invalid_number(self):
        with self.assertRaises(ValueError):
            to_words(4)

    def test_singular_words(self):
        expected = 'um real'
        words = to_words(1.00)
        self.assertEqual(words, expected)

    def test_plural_words(self):
        expected = 'dois reais'
        words = to_words(2.00)
        self.assertEqual(words, expected)

    def tes_cents_greater_than_zero(self):
        expected = 'mil reais e cinquenta e quatro centavos'
        words = to_words(1000.54)
        self.assertEqual(words, expected)

    def tes_cents_less_than_zero(self):
        expected = 'mil reais'
        words = to_words(1000.00)
        self.assertEqual(words, expected)
