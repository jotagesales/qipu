"""
@author:: Jotagê Sales
@organization: FS Company
@copyright: 2018 fs.com.br todos os direitos reservados.
"""
fake_page = b"""
<!DOCTYPE html>
<html>
<head>
	<div class="body" >
		<div role="main" class="main">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 order-sm-1">
						<h4 class="mb-0 heading-primary"><a href="?i=aerodromos&codigo=SBMT">ROTAER</a> 
						<small class="pull-right">
							<span class="badge badge-primary badge-xs">AMDT 45/18</span>
						</small>

					</h4>
					<p class="mb-2">
					</p>
					<div class="col-lg-4 order-sm-12">
						<h5 class="text-center heading-primary">Nascer/Por do Sol</h5>
						<div class="row">
							<div class="col-6 text-right">
								<h2 class="mb-0"><i class="fas fa-arrow-up"></i> <i class="fas fa-sun"></i></h2>
								<h4 class="mt-0"><?xml version="1.0" encoding="UTF-8"?><sunrise>08:16</sunrise></h4>
							</div>
							<div class="col-6 text-left">
								<h2 class="mb-0"><i class="far fa-sun"></i> <i class="fas fa-arrow-down"></i></h2>
								<h4 class="mt-0"><?xml version="1.0" encoding="UTF-8"?><sunset>21:24</sunset></h4>
							</div>
						</div>

						<div class="row">
							<div class="col">
								<p class="text-center"><a href="?i=aerodromos&p=sol&id=SBMT" 
								class="btn btn-xs btn-default">Ver Tabela</a></p>
							</div>
						</div>
						<div class="mt-0" id="location-map"></div>
						<hr class="mt-0">
						<p class="text-center mt-0"><img class="img-fluid" 
						src="https://www.aisweb.aer.mil.br/assets/templates/portal/img/logos/bt_redemet.png" alt=""></p>
						<h5 class="mb-0 heading-primary">METAR</h5>
						<p>082100Z 09008KT 5000 -RA BR BKN008 OVC012 18/16 Q1021=</p>
						<h5 class="mb-0 heading-primary">TAF</h5>
						<p>081616Z 0818/0906 12007KT 9000 BKN015TX18/0818Z TN16/0823ZPROB30 0818/0823 8000 DZ OVC010BECMG 0823/0901 BKN009 RMK PHM=
						</p>

						<hr>
						<h4 class="heading-primary">Cartas (3)</h4>
						<h4>ADC</h4>
						<ul class="list list-icons list-primary list-icons-style-2">

							<li><i class="fas fa-cloud-download-alt"></i> <a target="_blank" 
							onclick="javascript:pageTracker._trackPageview('/cartas/aerodromos');" href="http://ais.decea.gov.br/download/?arquivo=b44ec974-2ce5-4f75-bc8621c59d2d29a9&amp;apikey=1587263166">ADC SBMT</a>
							</li>

						</ul>
						<hr class="dashed">

						<h4>PDC</h4>
						<ul class="list list-icons list-primary list-icons-style-2">

							<li><i class="fas fa-cloud-download-alt"></i> <a target="_blank" 
							onclick="javascript:pageTracker._trackPageview('/cartas/aerodromos');" href="http://ais.decea.gov.br/download/?arquivo=25cc6e4f-ba05-4b05-b8a59a09a6db6617&amp;apikey=1587263166">PDC</a></li>

						</ul>
						<hr class="dashed">

						<h4>VAC</h4>
						<ul class="list list-icons list-primary list-icons-style-2">

							<li><i class="fas fa-cloud-download-alt"></i> <a target="_blank" 
							onclick="javascript:pageTracker._trackPageview('/cartas/aerodromos');" href="http://ais.decea.gov.br/download/?arquivo=413d90c8-bd47-1032-8701-72567f175e3a&amp;apikey=1587263166">
							VAC SBMT - RWY 12/30</a></li>

						</ul>
						<hr class="dashed">
					</div>	

				</div>
			</div>
		</div>
	</body>
	</html>
"""
