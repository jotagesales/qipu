"""
@author: Jotagê Sales
@date: 10/11/18
@organization: Jotagê Sales
@copyright: 2018 todos os direitos reservados.
"""
from num2words import num2words

default_language = 'pt_BR'


def to_words(number):
    if not isinstance(number, float):
        raise ValueError('The number {} is not a valid float'.format(number))

    first, second = [int(number) for number in str(number).split('.')]
    plural = 'reais' if first > 1 else 'real'

    first_part_words = num2words(first, lang=default_language)
    second_part_words = num2words(second, lang=default_language) if second > 0 else ''

    words = '{} {} e {} centavos'.format(first_part_words, plural,
                                         second_part_words) if first > 1 and second > 1 else '{} {}'.format(
        first_part_words, plural)
    return words
