# Qipu

### Importante ###
Para os passos seguintes, estou considerando que a maquina onde serão executados os comandos tem o docker instalado,
caso não tenha, nesse link estão todos os passos para instalação: https://docs.docker.com/install/.
É importante usar docker, pois assim garantimos que independente do SO usado, o ambiente será exatamente igual.

### Usando uma imagem docker pronta ###
```
docker pull jotage/qipu && docker tag jotage/qipu qipu
```
### Preparando a imagem docker manualmente ###
```
git clone https://jotagesales@bitbucket.org/jotagesales/qipu.git
cd qipu
docker build -t qipu .
```

### Executando testes unitários ###
```
docker run -it qipu make tests
```

### Executando script que imprime números por extenso ###
```
docker run -it qipu python qipu_script_1.py
```

### Executando script que consulta informações aeonáuticas ###
```
docker run -it qipu python qipu_script_2.py
```